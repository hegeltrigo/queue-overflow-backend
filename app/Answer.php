<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
     /**
     * Get all of the post's comments.
     */
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
}
