<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Hash;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     */
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login','signup']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        // return response()->json(['token' => $this->respondWithToken($token)]);
        return response()->json(['token' => $token, 'user' => auth()->user() ]);

    }

    public function signup()
    {
        $email = request('email');
        $password = Hash::make(request('password'));
        // $username = request('username');
        // $full_name = request('full_name');

        // $user = new User;
        // $user->email = $email;
        // $user->password = $password;
        // $user->name = $name;

        try {
            // $user.save();
            User::create([
                'email' => $email,
                'password' => $password,
                // 'username' => $username,
                // 'full_name' => $full_name,
            ]);
            return response()->json(["message" =>"Usuario creado exitosamente!"], 200);

          } catch (\Exception $e) {
            return response()->json($e, 401);
          }

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    public function payload()
    {
        return response()->json(auth()->payload());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }
}
